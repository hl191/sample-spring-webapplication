package at.home;

import at.home.api.ShoppingListApi;
import at.home.api.model.ShoppingItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ShoppingListRestController implements ShoppingListApi {

    @Autowired
    private ShoppingListService shoppingListService;

    @Override
    public ResponseEntity<List<ShoppingItem>> listShoppingItems() {
        return new ResponseEntity<>(shoppingListService.listShoppingItems(), HttpStatus.OK);
    }
}
