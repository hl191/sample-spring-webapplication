package at.home.model;

import at.home.api.model.ShoppingItem;

import java.util.function.Function;

public final class EntityToDtoMapper implements Function<ShoppingItemEntity, ShoppingItem> {

    @Override
    public ShoppingItem apply(final ShoppingItemEntity shoppingItemEntity) {
        return new ShoppingItem().id(shoppingItemEntity.getId())
                                 .name(shoppingItemEntity.getName())
                                 .priority(shoppingItemEntity.getPriority());
    }
}
