package at.home;

import at.home.api.model.ShoppingItem;
import at.home.model.EntityToDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ShoppingListService {

    private static final EntityToDtoMapper ENTITY_TO_DTO_MAPPER = new EntityToDtoMapper();

    @Autowired
    private ShoppingListRepository shoppingListRepository;

    public List<ShoppingItem> listShoppingItems() {
        return shoppingListRepository.findAll().stream().map(ENTITY_TO_DTO_MAPPER).collect(Collectors.toList());
    }
}
