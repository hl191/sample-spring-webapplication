insert into shopping_items(id, name, priority, created_at, updated_at) values (1, 'Zimt', 4, current_timestamp, current_timestamp);
insert into shopping_items(id, name, priority, created_at, updated_at) values (2, 'Joghurt', 3, current_timestamp, current_timestamp);
insert into shopping_items(id, name, created_at, updated_at) values (3, 'Obst', current_timestamp, current_timestamp);