#!/usr/bin/env bash
docker run --name myPostgres -e POSTGRES_DB=postgresDb -e POSTGRES_USER=user -e POSTGRES_PASSWORD=password -d -p 5432:5432 postgres
